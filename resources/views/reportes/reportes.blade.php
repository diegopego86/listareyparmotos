@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Reportes</div>

                <div class="panel-body">
                    <div class="list-group">
                    	<a href="{{ url('/reportes/lista_precios_por_grupos') }}" class="list-group-item">Lista de precios por grupos</a>
                    	<a href="{{ url('/reportes/lista_precios_anexos') }}" class="list-group-item">Generar lista de anexos</a>
                    	<a href="{{ url('/reportes/papelitos') }}" class="list-group-item">Memos bodega</a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
