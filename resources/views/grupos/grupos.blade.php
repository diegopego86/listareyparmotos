@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Grupos</div>

                <div class="panel-body">
                <ul>
                    @foreach($grupos as $grupo)
                        <li>{{ $grupo->nombre }}</li>
                    @endforeach
                </ul>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
