@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-xs-12">
            <div class="panel panel-default">
                <div class="panel-heading">Crear</div>

                <div class="panel-body">

                    @if (count($errors))
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                    @endif
                    
                    <form action="/grupos" method="POST">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <label for="grupo-nombre">Nombre:</label>
                            <input type="text" name="nombre" id="grupo-nombre" class="form-control" value="" required="required">
                        </div>
                        <div class="form-group">
                            <label for="grupo-logo">Logo:</label>
                            <input type="text" name="logo" id="grupo-logo" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label for="grupo-color">Color:</label>
                            <input type="text" name="color" id="grupo-color" class="form-control" value="">
                        </div>
                        <div class="form-group">
                            <label for="grupo-tipo-impresion">Tipo Impresion:</label>
                            <input type="text" name="tipo_impresion" id="grupo-tipo-impresion" class="form-control" value="">
                        </div>
                        <button type="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
