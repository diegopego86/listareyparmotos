<?php
use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('productos', function (Blueprint $table) {
            $table->increments('id');
            $table->string("codigo",12);
            $table->string("marca",100);
            $table->string("referencia",100);
            $table->string("descripcion");
            $table->decimal("precio",10,2);
            $table->boolean("agotado");
            $table->boolean("existencia");
            $table->boolean("orden");
            $table->integer("categoria_id")->unsigned();
            $table->integer("grupo_id")->unsigned();
            $table->integer("subgrupo_id")->unsigned();
            $table->timestamps();
            $table->index('codigo');
            $table->index('marca');
            $table->index('referencia');
            $table->index('descripcion');
            $table->foreign('categoria_id')
              ->references('id')->on('categorias')
              ->onDelete('cascade');
            $table->foreign('grupo_id')
              ->references('id')->on('grupos')
              ->onDelete('cascade');
            $table->foreign('subgrupo_id')
              ->references('id')->on('subgrupos')
              ->onDelete('cascade');            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('productos');
    }
}
