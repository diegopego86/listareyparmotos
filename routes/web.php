<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index');
Route::get('/productos','ProductosController@index');
Route::get('/reportes','ReportesController@index');
Route::get('/grupos','GruposController@index');
Route::get('/grupos/crear','GruposController@create');
Route::post('/grupos','GruposController@store');
Route::get('/grupos/{grupo}','GruposController@show');
Route::get('/grupos/{grupo}/editar','GruposController@edit');
Route::get('/subgrupos','SubgruposController@index');
Route::get('/categorias','CategoriasController@index');
Route::get('/configuracion','ConfiguracionController@index');